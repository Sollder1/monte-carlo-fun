package de.sollder1.montecarlofun;

import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

public class MonteCarloSim {

    private final GausSettings gausSettings;
    private final RandomDistributionSettings randomDistributionSettings;
    private final DepotSettings depotSettings;

    private double depotValue;
    private List<Double> percentageLookupTable;


    //Statistiken:
    private List<Double> monthlyMovements = new ArrayList<>();


    public MonteCarloSim(GausSettings gausSettings, RandomDistributionSettings randomDistributionSettings, DepotSettings depotSettings) {
        this.gausSettings = gausSettings;
        this.randomDistributionSettings = randomDistributionSettings;
        this.depotSettings = depotSettings;
    }

    public double run() {
        Map<Double, Double> distributions = generateDistributionTable();
        percentageLookupTable = buildLookupTable(distributions);
        depotValue = depotSettings.startAmount();
        for (int i = 0; i < depotSettings.monthsToSim(); i++) {
            simMonth();
        }
        //printStatistics();
        return depotValue;
    }

    private void printStatistics() {
        printSummary("Movements", monthlyMovements, value -> value * 12);
        System.out.printf("%,f %n", this.depotValue);
    }

    private void printSummary(String label, List<Double> data, ToDoubleFunction<Double> mapper) {
        DoubleSummaryStatistics summary = data.stream().collect(Collectors.summarizingDouble(mapper));
        System.out.printf("Avg-%s: %,f %n", label, summary.getAverage());
        System.out.printf("Min-%s: %,f %n", label, summary.getMin());
        System.out.printf("Max-%s: %,f %n", label, summary.getMax());
    }

    private void simMonth() {
        int movementIndex = ThreadLocalRandom.current().nextInt(0, percentageLookupTable.size());
        double movement = percentageLookupTable.get(movementIndex);
        monthlyMovements.add(movement * 100);
        depotValue += (depotValue * movement);
        depotValue += depotSettings.monthlyIn();
    }

    private List<Double> buildLookupTable(Map<Double, Double> distributions) {
        List<Double> temp = new ArrayList<>(randomDistributionSettings.lookupTableSize());
        double sum = distributions.values().stream()
                .collect(Collectors.summarizingDouble(Double::doubleValue))
                .getSum();
        double factor = 1 / sum;
        distributions.forEach((stepValue, distribution) -> {
            double amount = randomDistributionSettings.lookupTableSize() * (distribution * factor);
            for (int i = 0; i < amount; i++) {
                temp.add((stepValue / 12) / 100);
            }
        });
        Collections.shuffle(temp);
        return temp;
    }

    private Map<Double, Double> generateDistributionTable() {
        double counter = gausSettings.xMin;
        Map<Double, Double> result = new HashMap<>();
        while (true) {
            result.put(counter, calculateGaus(counter));
            counter += randomDistributionSettings.stepSize();
            if (counter > gausSettings.xMax()) {
                break;
            }
        }
        return result;
    }

    private double calculateGaus(double x) {
        double leftSide = 1 / (gausSettings.sigma() * Math.sqrt(2 * Math.PI));
        double exponent = -0.5 * Math.pow(((x - gausSettings.mu()) / gausSettings.sigma()), 2);
        return leftSide * Math.pow(Math.E, exponent);
    }


    public record GausSettings(double mu, double sigma, double xMin, double xMax) {
    }

    public record RandomDistributionSettings(double stepSize, int lookupTableSize) {
    }

    public record DepotSettings(double startAmount, double monthlyIn, int monthsToSim) {
    }

}

package de.sollder1.montecarlofun;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

public class Main {

    private static final int threads = 1;
    private static final int totalRuns = 1_000_000;


    public static void main(String[] args) throws Throwable {
        var runs = new ArrayList<Double>();

        final CountDownLatch countDownLatch = new CountDownLatch(threads);


        var start = System.currentTimeMillis();

        for (int t = 0; t < threads; t++) {
            new Thread(() -> {
                var runsLocal = new ArrayList<Double>();
                for (int i = 0; i < totalRuns / threads; i++) {
                    MonteCarloSim sim = new MonteCarloSim(gaus, vert, depot);
                    runsLocal.add(sim.run());
                    if (i > 0 && i % 10_000 == 0) {
                        System.out.println(Thread.currentThread().getName() + ": " + i);
                    }
                }
                synchronized (Main.class) {
                    runs.addAll(runsLocal);
                }
                countDownLatch.countDown();
            }).start();
        }
        countDownLatch.await();
        System.out.println(System.currentTimeMillis() - start);


        printSummary("Depotwert", runs, Double::doubleValue);
    }


    private static void printSummary(String label, Collection<Double> data, ToDoubleFunction<Double> mapper) {
        DoubleSummaryStatistics summary = data.stream().collect(Collectors.summarizingDouble(mapper));
        System.out.printf("Avg-%s: %,f %n", label, summary.getAverage());
        System.out.printf("Min-%s: %,f %n", label, summary.getMin());
        System.out.printf("Max-%s: %,f %n", label, summary.getMax());
    }


    private static MonteCarloSim.DepotSettings depot = new MonteCarloSim.DepotSettings(
            40_000.,
            1200.,
            12 * 40
    );

    private static MonteCarloSim.GausSettings gaus =
            new MonteCarloSim.GausSettings(
                    5,
                    3,
                    -10,
                    10
            );

    private static MonteCarloSim.RandomDistributionSettings vert = new MonteCarloSim.RandomDistributionSettings(
            0.25,
            1000
    );
}
